import re


def alternatingCharacters(s):
    ptn = r"([A-Z])(?!\1)"

    m2 = re.findall(ptn, s)

    result = len(s) - len(m2)
    return result
