def cat_and_mouse(x: int, y: int, z: int):
    distanceA = abs(x - z)
    distanceB = abs(y - z)

    if distanceA > distanceB:
        return 'Cat B'
    elif distanceA < distanceB:
        return 'Cat A'
    else:
        return 'Mouse C'
